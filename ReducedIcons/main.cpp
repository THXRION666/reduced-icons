#include "main.h"

void AsiPlugin::DrawIconHook(SRHook::CPU &cpu, RwVRect *&rect) {
	auto x = ( rect->x1 + rect->x2 ) * 0.5f;
	auto y = ( rect->y1 + rect->y2 ) * 0.5f;
	auto w = abs( rect->x1 - x ) * scaleX;
	auto h = abs( rect->y1 - y ) * scaleY;

	rect->x1 = x - w;
	rect->x2 = x + w;
	rect->y1 = y + h;
	rect->y2 = y - h;
}

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	drawIcon.onBefore += std::tuple{ this, &AsiPlugin::DrawIconHook };
	drawIcon.install();
}